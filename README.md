# MyLovelySurvey

How to process Google Forms results using Google Colab

## Files in this repository

* `My Lovely Survey.csv` - the raw responses CSV from the Google Forms
* `SurveyResults.ipynb` - the Google Colab notebook used to visualize the responses


